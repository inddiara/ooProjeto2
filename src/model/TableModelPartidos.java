/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import edu.unb.fga.dadosabertos.Partido;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author indiara
 */
public class TableModelPartidos extends AbstractTableModel{
    private final String [] partidoHeader = {"Nome","Total"};
    private final List<Partido> partidos;
    
    public TableModelPartidos(HashMap<String,Integer> partidos){
        this.partidos = partidoSomaList(partidos);
    }
    
    @Override
    public int getColumnCount(){
        return partidoHeader.length;
    }
    
    @Override 
    public int getRowCount(){
        
        return partidos.size();
    }
    
    @Override
    public Object getValueAt(int rowIndex,int columnIndex){
        //System.out.println(rowIndex+"\n"+partidos.get(rowIndex));
        PartidoSoma partido = (PartidoSoma) partidos.get(rowIndex);
        
        switch (columnIndex){
            case 0:
                return partido.getNome();
            case 1:
                return partido.getSoma();
            }
            return null;
        }
    @Override
    public String getColumnName(int coluna){
        return this.partidoHeader[coluna];
    }
    
    private class PartidoSoma extends Partido implements Comparable<PartidoSoma>{
        private final Integer soma;
        public PartidoSoma(String nome,Integer soma){
            super.nome = nome;
            this.soma = soma;
        }

        public Integer getSoma() {
            return soma;
        }

        @Override
        public int compareTo(PartidoSoma o) {
            return super.nome.compareToIgnoreCase(o.getNome());
        }
    }
    private ArrayList<Partido> partidoSomaList(HashMap<String,Integer> partidos){
        ArrayList<Partido> listaPartidos = new ArrayList<Partido>();
        for (String nome : partidos.keySet()){
            listaPartidos.add(new PartidoSoma(nome, partidos.get(nome)));
        }
        return listaPartidos;
    }
    public List<Partido> getNomePartidos(){
        return partidos;
    }
}
