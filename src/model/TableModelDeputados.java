/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import edu.unb.fga.dadosabertos.Deputado;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author indiara
 */
public class TableModelDeputados extends AbstractTableModel {
    public static Integer CAMPONOME = 0, CAMPOPARTIDO=1,CAMPOESTADO=2,CAMPOEMAIL=3,CAMPOTELEFONE=4,CAMPOCONDICAO=5;
    private final List<Deputado> deputados;
    private final String[] colunas = {"Nome","Partido","Estado","Email","Telefone","Condição"};
    
    public TableModelDeputados(List<Deputado> deputados){
        this.deputados = deputados;
    }
    
    @Override
    public int getColumnCount(){
        return colunas.length;
    }
    
    @Override 
    public int getRowCount(){
        
        return deputados.size();
    }
    
    @Override
    public Object getValueAt(int rowIndex,int columnIndex){
       // System.out.println(rowIndex+"\n"+deputados.get(rowIndex));
        Deputado d = (Deputado) deputados.get(rowIndex);
        
        switch (columnIndex){
            case 0:
                return d.getNome();
            case 1:
                return d.getPartido();
            case 2:
                return d.getUf();
            case 3:
                return d.getEmail();
            case 4:
                return d.getFone();
            case 5:
                return d.getCondicao();
            }
            return null;
        }
    @Override
    public String getColumnName(int coluna){
        return this.colunas[coluna];
    }
    
}
