/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import view.Tela;

/**
 *
 * @author indiara
 */
public class Principal {
    public static void main(String args[]){
        Camara cm = new Camara();
        try {
            cm.obterDados();
        } catch (JAXBException | IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        List <Deputado> deputados = cm.getDeputados();
//        for (Deputado dp :  deputados){
//            System.out.println(dp.getNome());
//        }
//        Collections.sort(deputados,new OrdenaNome());
//        Collections.sort(deputados,new OrdenaEmail());
//        for (Deputado dp :  deputados){
//            System.out.println(dp.getNome());
//        }
     //   System.out.println(dp.getNome());
//        List<Deputado> deputados = CamaraUtil.deletMe(30);
//        new Partido();
//        System.out.println("int ideCadastro, int matricula, String condicao, String nome, \n"
//                + "String nomeParlamentar, String urlFoto, String sexo, String uf,\n"
//                + " String partido, String gabinete, String anexo, String fone, \n"
//                + "String email, List<Comissao> comissoes\n\n"
//                + "int idOrgaoLegislativoCD, String siglaComissao, String nomeComissao,"
//                + " String condicaoMembro, String dataEntrada, String dataSaida\n\n, "
//                + "Detalhes detalhes\n\nint numLegislatura, String situacaoNaLegislaturaAtual, \n"
//                + "String ufRepresentacaoAtual, String dataNascimento, String dataFalecimento, \n"
//                + "Partido partido\n\ntring idPartido, String sigla, String nome");
        HashMap<String,Integer> partidoSoma = CamaraUtil.contaPartidos(deputados);
        
        Tela.iniciaJanela(deputados, partidoSoma);
    }
    
}
