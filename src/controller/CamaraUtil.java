/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import edu.unb.fga.dadosabertos.Comissao;
import edu.unb.fga.dadosabertos.Deputado;
import edu.unb.fga.dadosabertos.Detalhes;
import edu.unb.fga.dadosabertos.Partido;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author indiara
 */
public class CamaraUtil {
    public static HashMap<String,Integer> contaPartidos(List<Deputado> deputados){
        HashMap<String,Integer> somaPartidos = new HashMap<String, Integer>();
      //  ArrayList<String> partido = new ArrayList<String>();
        for (Deputado deputado : deputados){
            String partido = deputado.getPartido();
            if (somaPartidos.containsKey(partido)){
                Integer soma = somaPartidos.get(partido);
                soma++;
                somaPartidos.put(partido, soma);
            }else{
                somaPartidos.put(partido,1);
            }
        }
        return somaPartidos;
    } 
    public static ArrayList<Deputado> deletMe(int number){
        ArrayList<Deputado> deputados = new ArrayList<>();
        while(number-- > 0){
        deputados.add(new Deputado(number, 10, "condicao", "nome"+number, "nomeParlamentar", "urlFoto", "sexo", "uf", "partido"+(number%4),
                "gabinete", "anexo", "fone", "email"+number, new ArrayList<Comissao>(),
                new Detalhes(1, "situacaoNaLegislaturaAtual", "ufRepresentacaoAtual", "dataNascimento", "dataFalecimento",
                        new Partido("idPartido","sigla", "nome"))));
        }
        return deputados;
    }
}
