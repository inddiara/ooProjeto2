/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import edu.unb.fga.dadosabertos.Partido;
import java.util.Comparator;

/**
 *
 * @author indiara
 */
public class OrdenaPartido implements Comparator<Partido> {

    @Override
    public int compare(Partido o1, Partido o2) {
        return o1.getNome().compareToIgnoreCase(o2.getNome());
    }
    
}
