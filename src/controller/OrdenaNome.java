/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import edu.unb.fga.dadosabertos.Deputado;
import java.util.Comparator;

/**
 *
 * @author indiara
 */
public class OrdenaNome implements Comparator<Deputado>{

    @Override
    public int compare(Deputado o1, Deputado o2) {
        return o1.getNome().compareToIgnoreCase(o2.getNome());
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
    
}
